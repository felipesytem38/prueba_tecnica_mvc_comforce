// Funciones para la validación de campos de contraseña

$(document).ready(function() {
    //variables
    var password1 = $('[name=password1]');
    var password2 = $('[name=password2]');
    var confirmacion = "La contraseña coincide";
    var longitud = "La contraseña debe tener un mínimo de 6 carácteres";
    var negacion = "No coincide la contraseña";
    var vacio = "La confirmación de contraseña no puede estar vacía";
    // caracteristica y ubicación  del elemento span
    var span = $('<span></span>').insertAfter(password2);
    span.hide();
    //función que comprueba las dos contraseñas
    function coincidePassword(){
      var valor1 = password1.val();
      var valor2 = password2.val();
      //muestro el span
      span.show().removeClass();
      //condiciones dentro de la función
        if(valor1 != valor2){
        span.text(negacion).addClass('negacion');
        }
        if(valor2.length==0 || valor2==""){
        span.text(vacio).addClass('negacion');
        }
        if(valor1.length<6){
        span.text(longitud).addClass('negacion');
        }
        if(valor1.length!=0 && valor1==valor2){
        span.text(confirmacion).removeClass("negacion").addClass('confirmacion');
        }
    }
    //ejecuto la función al soltar la tecla
    password2.keyup(function(){
      coincidePassword();
      });
    password1.keyup(function(){
      coincidePassword();
      });
});

// Funciones para la validación de campos de contraseña de usuario en sesión

$(document).ready(function() {
    //variables
    var password1 = $('[name=nueva-contraseña]');
    var password2 = $('[name=nueva-contraseña_confirmation]');
    var confirmacion = "La contraseña coincide";
    var longitud = "La contraseña debe tener un mínimo de 6 carácteres";
    var negacion = "No coincide la contraseña";
    var vacio = "La confirmación de la contraseña no puede estar vacía";
    // caracteristica y ubicación  del elemento span
    var span = $('<span></span>').insertAfter(password2);
    span.hide();
    //función que comprueba las dos contraseñas
    function coincidePassword(){
      var valor1 = password1.val();
      var valor2 = password2.val();
      //muestro el span
      span.show().removeClass();
      //condiciones dentro de la función
        if(valor1 != valor2){
        span.text(negacion).addClass('negacion');
        }
        if(valor2.length==0 || valor2==""){
        span.text(vacio).addClass('negacion');
        }
        if(valor1.length<6){
        span.text(longitud).addClass('negacion');
        }
        if(valor1.length!=0 && valor1==valor2 && valor2==valor1){
        span.text(confirmacion).removeClass("negacion").addClass('confirmacion');
        }
    }
    //ejecuto la función al soltar la tecla
    password2.keyup(function(){
      coincidePassword();
      });
    password1.keyup(function(){
      coincidePassword();
      });
    });

// Funcion para permitir solo números en los input
function validaNum(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

// Funcion para permitir solo letras en los input
function soloLetras(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = "8-37-39-46";

    tecla_especial = false
    for(var i in especiales){
        if(key == especiales[i]){
            tecla_especial = true;
            break;
        }
    }
    if(letras.indexOf(tecla)==-1 && !tecla_especial){
        return false;
    }
}
