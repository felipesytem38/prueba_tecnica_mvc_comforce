<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Date\Date;
use App\Models\Proceso;
use App\Models\Sede;
use App\User;


class ProcesosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // consulta de procesos por id de usuario en sesión
        $procesos = Proceso::orderBy('id','ASC')
                            ->where('user_id', '=', $id = Auth::id())
                            ->get();
        return view('procesos.list-procesos', compact('procesos'));
    }

    public function listar_por_usuarios()
    {
        // consultar procesos de todos los usuarios
        $procesos = Proceso::orderBy('id','ASC')->get();
        return view('procesos.list-procesos-usuarios', compact('procesos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // consulta a la tabla sedes
        $sedes = Sede::all();
        // redirección a vista de crear proceso, pasando la informacion de sedes
        return \View::make('procesos.new-proceso', compact('sedes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // guardar los datos del proceso
        $proceso = new \App\Models\Proceso();
        // datos traidos de la vista
        $proceso->numero_proceso = $request->numero_proceso;
        $proceso->descripcion = $request->descripcion;
        $proceso->fecha_creacion = $request->fecha_creacion;
        $proceso->sede_id = $request->sede_id;
        $proceso->presupuesto = $request->presupuesto;
        $proceso->user_id = Auth::id();

        // validacion de campos (solo en caso de no funcionar las validaciones de la vista)
        $validatedData = $request->validate([
            'numero_proceso' => 'required|string|min:8|max:8',
            'descripcion' => 'required|string|max:200',
            'fecha_creacion' => 'required',
            'sede_id' => 'required',
            'presupuesto' => 'required|max:20'
        ]);

        // acción guardar y retorno de mensajes
        if($proceso->save()) {
          return redirect('/procesos')->with('msj','Proceso registrado correctamente.');
         }else{
          return redirect('/procesos')->with('msjerror','Ha sucedido un error al registrar el Proceso.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // redirección a la vista editar proceso con la información de la tabla sedes
        $proceso = Proceso::find($id);
        $sedes = Sede::all();
        return \View::make('procesos.update-proceso', compact('proceso', 'sedes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // actualizar información del proceso
        $proceso = Proceso::find($id);

        $proceso->numero_proceso = $request->numero_proceso;
        $proceso->descripcion = $request->descripcion;
        $proceso->fecha_creacion = $request->fecha_creacion;
        $proceso->sede_id = $request->sede_id;
        $proceso->presupuesto = $request->presupuesto;
        $proceso->user_id = Auth::id();

        // validacion de campos (solo en caso de no funcionar las validaciones de la vista)
        $validatedData = $request->validate([
            'numero_proceso' => 'required|string|min:8|max:8',
            'descripcion' => 'required|string|max:200',
            'fecha_creacion' => 'required',
            'sede_id' => 'required',
            'presupuesto' => 'required|max:20'
        ]);

        // actualziación en base de datos y envio de mensajes
        if ($proceso->save()) {
          return redirect('/procesos')->with('msj','Información del Proceso actualizada correctamente.');
         }else{
          return redirect('/procesos')->with('msjerror','Ha sucedido un error al actualizar la Información del Proceso.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
