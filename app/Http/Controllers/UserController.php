<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Hash;
use App\User;
use App\Role;

class UserController extends Controller
{

  public function index()
    {
        // listar usuarios
        $users = User::orderBy('id','ASC')
                       ->where('id', '!=', $id = Auth::id())
                       ->get();
        // retornar a la vista
        return view('users.list-users', compact('users'));
    }

    public function edit($id)
    {
        // buscar datos de usuario por id
        $users = User::find($id);
        // retornar consulta a la vista de actualizar datos
        return \View::make('users.update-user', compact('users'));
    }
    public function update(Request $request, $id)
    {
        // actualziar datos
        $user = User::find($id);

        $user->name = $request->name;
        $user->email = $request->email;
        // mensajes de acción
        if ($user->save()) {
          return redirect('/users')->with('msj','Usuario actualizado correctamente.');
         }else{
          return redirect('/users')->with('msjerror','Ha sucedido un error al actualizar los datos de usuario.');
        }
    }

    // editar datos de usuario en sesión
    public function editSessionUser($id)
    {
        // Recibe el parametro id del registro a modificar
        $users = User::find($id);
        return \View::make('users.update-session-user', compact('users'));
    }

    public function updateSessionUser(Request $request, $id)
      {
          // actualizar perfil de usuario
          $user = User::find($id);

          $user->name = $request->name;
          $user->email = $request->email;

          if ($user->save()) {
            return redirect('/home')->with('msj','Sus datos de perfil de usuario han sido actualizados correctamente.');
           }else{
            return redirect('/home')->with('msjerror','Ha sucedido un error al actualizar los datos de su perfil de usuario.');
          }
      }

      // vista para cambiar contraseña
      public function changeUserPassword(){
          return \View::make('users.change-user-password');
      }

      // función para guardar el cambio de la contraseña
      public function saveNewPassword(Request $request){

          if (!(Hash::check($request->get('contraseña-actual'), Auth::user()->password))) {
              // contraseña actual no coincide
              return redirect()->back()->with("msjerror","Su contraseña actual no coincide con la ingresada recientemente, por favor revise e intente nuevamente.");
          }

          if(strcmp($request->get('contraseña-actual'), $request->get('nueva-contraseña')) == 0){
              // contraseña actual y contraseña nueva son iguales
              return redirect()->back()->with("msjerror","su nueva contraseña no puede ser igual a la actual, por favor ingrese una contraseña diferente.");
          }

          $validatedData = $request->validate([
              'contraseña-actual' => 'required',
              'nueva-contraseña' => 'required|string|min:6|confirmed',
          ]);

          //Change Password
          $user = Auth::user();
          $user->password = bcrypt($request->get('nueva-contraseña'));
          $user->save();

          return redirect('/home')->with("msj","Su contraseña de ingreso se actualizó correctamente en el sistema!");

      }

}
