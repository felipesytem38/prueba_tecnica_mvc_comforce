<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proceso extends Model
{
    // modelo de la tabla procesos
    protected $table = "procesos";

    // foraneas
    public function Sede(){
      return $this->hasOne('App\Models\sede','id','sede_id');
    }

    public function User(){
      return $this->hasOne('App\User','id','user_id');
    }
}
