<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    // relacion entre el modelo user y role
    public function users()
    {
        return $this
            ->belongsToMany('App\User')
            ->withTimestamps();
    }
}
