El Sistema tiene dos tipos de usuario, el usuario normal que se puede registrar de manera simple, y el usuario administrador, ya creado en la base de datos
desde el proyecto, el usuario administrador solo puede editar los datos de los usuarios, y listar los procesos que han creado todos los usuarios.
Las credenciales para el administrador son: 
correo: administrador@gmail.com
contraseña: 123456

Para ejecutar el proyecto local es nesario usar composer, y configurar el .env con las credenciales de mysql del equipo y el  nombre de la base de datos.
Para restaurar la base de datos solo es necesario importarla desde el phpmyadmin,o copiando el codigo del interior del archivo en la consola del phpmyadmin.

Proyecto y base de datos creados por Juan Felipe Moreno Osuna.