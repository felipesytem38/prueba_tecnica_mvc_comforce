<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

/* Rutas del Administrador */
Route::group(['middleware' => 'admin'], function () {
    Route::resource('users', 'UserController');
    Route::get('users', 'UserController@index')->name('users');

    Route::get('listar_por_usuarios', 'ProcesosController@listar_por_usuarios')->name('listar_por_usuarios');
});

/* Rutas Generales */
Route::group(['middleware' => 'auth'], function () {
    /* Página principal */
    Route::get('/home', 'HomeController@index')->name('home');
    /* Rutas de procesos */
    Route::resource('procesos', 'ProcesosController');
    Route::get('procesos', 'ProcesosController@index')->name('procesos');
    /* Rutas de Perfil de usuario */
    Route::resource('user', 'UserController');
    Route::get('edit-user, {id}', 'UserController@editSessionUser')->name('edit-user');
    Route::put('update-user, {id}', 'UserController@updateSessionUser')->name('update-user');
    Route::get('change-user-password','UserController@changeUserPassword')->name('change-user-password');
    Route::post('saveNewPassword','UserController@saveNewPassword')->name('saveNewPassword');
});
