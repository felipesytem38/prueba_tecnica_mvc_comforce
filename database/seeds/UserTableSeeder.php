<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
         // consulta de los roles y uso en variables
         $role_user = Role::where('nombre', 'user')->first();
         $role_admin = Role::where('nombre', 'admin')->first();

         // creacion de usuario
         $user = new User();
         $user->name = 'usuario de prueba';
         $user->email = 'user@gmail.com';
         $user->password = bcrypt('123456');
         $user->save();
         $user->roles()->attach($role_user);

         // creacion de administrador
         $user = new User();
         $user->name = 'administrador del sistema';
         $user->email = 'administrador@gmail.com';
         $user->password = bcrypt('123456');
         $user->save();
         $user->roles()->attach($role_admin);
      }
}
