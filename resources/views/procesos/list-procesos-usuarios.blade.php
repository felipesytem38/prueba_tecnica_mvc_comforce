@extends('layouts.app')
@section('content')
            <div class="container" style="margin-top: 2px; text-align: center;">
              @if(session('msj'))
                  <div class="alert alert-success alert-dismissible fade show" role="alert">
                          {{session('msj')}}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
              @endif
              @if(count($errors)>0)
                  @foreach($errors->all() as $error)
                      <div class="alert alert-danger alert-dismissible fade show" role="alert">
                              {{$error}}
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                  @endforeach
              @endif
            </div>
            <!-- /.site-header -->
            <div class="site-content">
            <div class="demo-wrapper">
                <!-- /.demo-header -->
                <div class="bd-content">
                        <h2 class="mb-4" style="text-align: center; font-size: 30px;">Procesos Por Usuario</h2>
                </div>
                <a href="{{ route('procesos.create') }}" id="botones" class="btn btn-primary btn-registrar">Registrar Proceso
                  <i class="material-icons iconos">add_to_photos</i></a>
                <div class="tablas">
                  <table class="table table-bordered table-striped" id="tabla" >
                  <thead class="thead-dark">
                        <th style="text-align: center;">Número de Proceso</th>
                        <th style="text-align: center;">Descripción</th>
                        <th style="text-align: center;">Fecha de Creación del Proceso</th>
                        <th style="text-align: center;">Sede</th>
                        <th style="text-align: center;">Presupuesto en Pesos Colombianos</th>
                        <th style="text-align: center;">Presupuesto en Dolares</th>
                        <th style="text-align: center;">Nombre Usuario</th>
                    </tr>
                  </thead>
                    <tbody>
                      @foreach($procesos as $proceso)
                      <tr>
                        <td>{{$proceso->numero_proceso}}</td>
                        <td>{{$proceso->descripcion}}</td>
                        <td>{{Date::parse($proceso->fecha_creacion)->format('d / F / Y')}}</td>
                        <td>{{$proceso->Sede->nombre}}</td>
                        <td>${{number_format($proceso->presupuesto, 0, '', '.')}}</td>
                        <td>USD {{number_format($proceso->presupuesto*0.00032, 0, '', '.')}}</td>
                        <td>{{$proceso->User->name}}</td>
                      </tr>
                     @endforeach
                  </tbody>
                </table>
                <script>
                  $(document).ready(function(){
                  $('#tabla').DataTable( {
                      "lengthMenu": [[5, 10, 15], [5, 10, 15]],
                      language: {
                            "emptyTable": "No hay procesos registrados actualmente!",
                            "info": "Mostrando de _START_ a _END_ de un total de _TOTAL_ registros",
                            "infoEmpty": "Mostrando 0 a 0 de 0 Registros",
                            "infoFiltered": "(Filtrado de un total de _MAX_ total registros)",
                            "lengthMenu": "Mostrar _MENU_ Registros",
                            "loadingRecords": "Cargando...",
                            "processing": "Procesando...",
                            "search": "Buscar:",
                            "zeroRecords": "Sin resultados encontrados",
                            "paginate": {
                            "first": "Primero",
                            "last": "Ultimo",
                            "next": "Siguiente",
                            "previous": "Anterior"
                          }
                      },
                    });
                  });
                </script>
                </div>
              </div>
              </div>

            <!-- /.site-footer -->
            <footer class="site-footer">
                <div class="mr-auto">
                  Prueba PHP - Juan Moreno
                </div>
            </footer>

@endsection
