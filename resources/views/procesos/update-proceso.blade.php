@extends('layouts.app')
@section('content')
	<section class="container">
			<div style="margin-top: 18px;">
				<h2 style="text-align: center;">Actualizar Información del Proceso</h2>
					<form method="post" style="margin-top: 20px; margin-bottom: 3%; padding-left: 36%;" action="{{ route('procesos.update', $proceso->id)}}" autocomplete="off">
						@csrf
						@method('PUT')
						<label for="numero_proceso">Número del Proceso</label>
						<input name="numero_proceso" type="text" style="width: 300px;" class="form-control" value="{{$proceso->numero_proceso}}" placeholder="el número debe tener 8 caracteres exactos" maxlength="8" minlength="8" required>
						<label for="descripcion">Descripción</label>
						<textarea name="descripcion" maxlength="200" style="width: 300px;" class="form-control" placeholder="ingrese máximo 200 caracteres" required>{{$proceso->descripcion}}</textarea>
						<label for="fecha" class="control-label">Fecha de Creación</label>
							<div class="input-group date"><span class="input-group-addon"><i class="material-icons dp48">insert_invitation</i></span>
								<input type="text" name="fecha_creacion" value="{{$proceso->fecha_creacion}}" class="input_icono" id="datepicker" required/>
							</div>
						<label for="sede_id">Sede</label>
						<div class="input-group"><span class="input-group-addon"><i class="material-icons dp48">location_on</i></span>
							<select class="input_icono" name="sede_id">
										@foreach($sedes as $sede)
										<option value="{{$sede->id}}" {{$proceso->sede_id==$sede->id? 'selected': null }}>{{$sede->nombre}}</option>
										@endforeach
							</select>
						 </div>
						<label for="presupuesto">Presupuesto COP (En pesos colombianos)</label>
						<div class="input-group"><span class="input-group-addon"><i class="material-icons dp48">attach_money</i></span>
							<input name="presupuesto" type="text" value="{{$proceso->presupuesto}}" class="input_icono" onkeypress="return validaNum(event)"
							title="por favor ingrese la suma sin puntos, comas, espacios, ni caracteres especiales" maxlength="20" required />
						</div>

						<a href="{{ route('procesos') }}" class="btn btn-danger" style="margin-top: 20px;">Cancelar</a>
						<input type="submit" value="Actualizar" style="margin-top: 20px;" class="btn btn-success">
					</form>
			</div>
	</section>
@endsection
