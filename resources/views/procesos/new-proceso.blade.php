@extends('layouts.app')
@section('content')
	<section class="container">
			@if(session('msjerror'))
					<div class="alert alert-warning alert-dismissible fade show" role="alert" style="text-align: center;">
									{{session('msjerror')}}
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
					</div>
			@endif
			<div style="margin-top: 5px;">
				<h2 style="text-align: center;">Registrar Proceso</h2>
					<form style="margin-top: 8px; margin-bottom: 3%; padding-left: 36%;" method="post" action="{{route('procesos.store')}}" autocomplete="off">
						{{csrf_field()}}

						<label for="numero_proceso">Número del Proceso</label>
						<input name="numero_proceso" type="text" style="width: 300px;" class="form-control" placeholder="el número debe tener 8 caracteres exactos" maxlength="8" minlength="8" required>
						<label for="descripcion">Descripción</label>
						<textarea name="descripcion" maxlength="200" style="width: 300px;" class="form-control" placeholder="ingrese máximo 200 caracteres" required></textarea>
						<label for="fecha" class="control-label">Fecha de Creación</label>
							<div class="input-group date"><span class="input-group-addon"><i class="material-icons dp48">insert_invitation</i></span>
								<input type="text" name="fecha_creacion" value="{{Date::now()->format('Y-m-d')}}" class="input_icono" readonly required/>
							</div>
						<label for="sede_id">Sede</label>
						<div class="input-group"><span class="input-group-addon"><i class="material-icons dp48">location_on</i></span>
						<select class="input_icono" name="sede_id" required>
									<option value="">seleccione...</option>
									@foreach($sedes as $sede)
									<option value="{{$sede->id}}">{{$sede->nombre}}</option>
									@endforeach
				     </select>
						 </div>
						<label for="presupuesto">Presupuesto COP (En pesos colombianos)</label>
						<div class="input-group"><span class="input-group-addon"><i class="material-icons dp48">attach_money</i></span>
							<input name="presupuesto" type="number" class="input_icono" onkeypress="return validaNum(event)"
							title="por favor ingrese la suma sin puntos, comas, espacios, ni caracteres especiales" maxlength="20" required />
						</div>

					  <a  href="{{ route('procesos') }}" class="btn btn-danger" style="margin-top: 20px; margin-left: 8%;">Cancelar</a>
						<input type="submit" value="Registrar" style="margin-top: 20px;" class="btn btn-success">
					</form>
			</div>
	</section>

@endsection
