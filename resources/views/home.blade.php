@extends('layouts.app')

@section('content')
<div class="container" style="margin-top: 2px; text-align: center;">
  @if(session('msj'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
              {{session('msj')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
      </div>
  @endif
  @if(count($errors)>0)
      @foreach($errors->all() as $error)
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  {{$error}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
          </div>
      @endforeach
  @endif
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">¡Bienvenido!</div>

                <div class="card-body">
                  @if(Auth::user()->hasRole('admin'))
                      <div style="margin-bottom: 10px;">Acceso como administrador</div>
                      <a id="botones" class="btn btn-primary " href="{{ route('listar_por_usuarios') }}">
                        Ver Procesos Registrados <i class="material-icons iconos">remove_red_eye</i></a>
                  @else
                      <div style="margin-bottom: 10px;">Acceso usuario</div>
                      <a id="botones" class="btn btn-primary " href="{{ route('procesos') }}">
                        Ver mis Procesos Registrados <i class="material-icons iconos">remove_red_eye</i></a>
                  @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
