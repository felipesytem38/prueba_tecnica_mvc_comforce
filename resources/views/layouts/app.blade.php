<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Sistema de Procesos</title>

    <!-- Jquery -->
    <script type="text/javascript" src="//code.jquery.com/jquery-1.12.3.js"></script>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/Funciones.js') }}" defer></script>

    <!-- Librerías js -->
    <script type="text/javascript" src="{{ asset('js/librerias/bootstrap-datepicker.min.js') }}" defer></script>
    <script type="text/javascript" src="{{ asset('js/librerias/jquery.dataTables.min.js') }}" defer></script>
    <script type="text/javascript" src="{{ asset('js/librerias/bootstrap-datepicker.es.js') }}" defer></script>

    <!-- Librerías css -->
    <link rel="stylesheet" href="{{ asset('css/librerias/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/librerias/bootstrap-datepicker.css') }}">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/Style.css') }}" />

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('home') }}">
                    Sistema de Procesos <i class="material-icons iconos">home</i></a>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}
                                <i class="menu-icon material-icons dp48" style="vertical-align: middle;">vpn_key</i></a>
                            </li>
                            <li class="nav-item">
                                @if (Route::has('register'))
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Registrarme') }}
                                    <i class="menu-icon material-icons dp48" style="vertical-align: middle;">person_add</i></a>
                                @endif
                            </li>
                        @else
                        @if(Auth::user()->hasRole('admin'))
                          <li class="nav-item dropdown" >
                            <a class="nav-link desplegable" href="{{ route('users') }}" >
                            <i class="menu-icon material-icons dp48" style="vertical-align: sub;">group</i> <span class="menu-text">Usuarios</span></a>
                          </li>
                          <li class="nav-item dropdown" >
                            <a class="nav-link desplegable" href="{{ route('listar_por_usuarios') }}" >
                            <i class="menu-icon material-icons dp48" style="vertical-align: sub;">list</i> <span class="menu-text">Procesos</span></a>
                          </li>
                        @endif
                        @if(Auth::user()->hasRole('user'))
                        <li class="nav-item dropdown" >
                          <a class="nav-link desplegable" href="{{ route('procesos') }}" >
                          <i class="menu-icon material-icons dp48" style="vertical-align: sub;">list</i> <span class="menu-text">Procesos</span></a>
                        </li>
                        @endif

                        <li class="nav-item dropdown" style="margin-top: 8px;">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle desplegable" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->nombres }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" >

                                <a class="nav-link desplegable" title="Editar mis datos básicos" href="{{ route('edit-user', Auth::user()->id) }}" >
                                <i class="menu-icon material-icons dp48" style="vertical-align: middle;">person_pin</i>
                                <span style="margin-Left: 5px;">Mi perfil</span></a>

                                <a class="nav-link desplegable" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                   <i class="menu-icon material-icons dp48" style="vertical-align: middle">exit_to_app</i>
                                    {{ __('Cerrar Sesión') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
