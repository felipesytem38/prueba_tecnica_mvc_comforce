@extends('layouts.app')
@section('content')
	<section class="container">
			<div style="margin-top: 8px;">
				<h2 style="text-align: center;">Modificar Usuario</h2>
					<form method="post" style="margin-top: 20px; margin-bottom: 3%; padding-left: 36%;" action="{{ route('users.update', $users->id)}}" autocomplete="off">
						@csrf
						@method('PUT')
						<label for="name">Nombre Completo</label>
						<input name="name" type="text" value="{{$users->name}}" size="20" style="width: 300px;" class="form-control" onkeypress="return soloLetras(event)">
						<label for="email">Correo Electrónico</label>
						<input name="email" type="email" value="{{$users->email}}" size="20" style="width: 300px;" class="form-control">

						<a href="{{ route('users') }}" class="btn btn-danger" style="margin-top: 20px;">Cancelar</a>
						<input type="submit" value="Actualizar" style="margin-top: 20px;" class="btn btn-success">
					</form>
			</div>
	</section>
@endsection
