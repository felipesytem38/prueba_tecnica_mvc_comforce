@extends('layouts.app')
@section('content')
	<section class="container">
			<div style="margin-top: 18px;">
				<h3 style="text-align: center;">Modificar Mis Datos De Usuario</h3>
					<form method="post" style="margin-top: 20px; margin-bottom: 3%; padding-left: 36%;" action="{{ route('update-user', $users->id)}}" autocomplete="off">
						@csrf
						@method('PUT')
						<label for="name">Nombre Completo</label>
						<input name="name" type="text" value="{{$users->name}}" size="20" style="width: 300px;" class="form-control" onkeypress="return soloLetras(event)">
						<label for="email">Correo Electrónico</label>
						<input name="email" type="text" value="{{$users->email}}" size="20" style="width: 300px;" class="form-control">

						<a href="{{ route('home') }}" class="btn btn-danger" style="margin-top: 20px;">Cancelar</a>
						<input type="submit" value="Actualizar" style="margin-top: 20px;" class="btn btn-success">
					</form>
					<a href="{{ route('change-user-password') }}" class="btn btn-primary" id="botones" style="margin-left: 37%; width: 280px; border-radius: 28px;">Actualizar Mi Contraseña
					<i class="material-icons iconos">lock</i></a>
			</div>
	</section>
@endsection
