@extends('layouts.app')
@section('content')
        <div class="container" style="margin-top: 2px; text-align: center;">
          @if(session('msj'))
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                      {{session('msj')}}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
              </div>
          @endif
          @if(count($errors)>0)
              @foreach($errors->all() as $error)
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                          {{$error}}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
              @endforeach
          @endif
        </div>
        <div class="site-content">
            <div class="demo-wrapper">
                <div class="bd-content">
                        <h2 style="text-align: center; font-size: 30px;">Usuarios</h2>
                </div>
                <form class="tablas">
                <div class="table-container">
                <table class="table table-bordered table-striped" style="text-align: center" id="tabla">
                  <thead style="text-align: center" class="thead-dark">
                        <th style="text-align: center">Nombre Completo</th>
                        <th style="text-align: center">Email</th>
                        <th></th>
                    </tr>
                  </thead>
                    <tbody>
                      @foreach($users as $user)
                      <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                          <a class="btn btn-primary btn-xs" id="botones" title="Editar Usuario" href="{{ route('users.edit', $user->id) }}"><i class="material-icons">edit</i></a>
                        </td>
                      </tr>
                     @endforeach
                  </tbody>
                </table>
                </form>
            </div>
          </div>
        </div>
        <script>
          $(document).ready(function(){
          $('#tabla').DataTable( {
              "lengthMenu": [[5, 10, 15], [5, 10, 15]],
              language: {
                    "emptyTable": "No hay donaciones de personas registradas actualmente!",
                    "info": "Mostrando de _START_ a _END_ de un total de _TOTAL_ registros",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
                    "infoFiltered": "(Filtrado de un total de _MAX_ total registros)",
                    "lengthMenu": "Mostrar _MENU_ Registros",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                  }
              },
            });
          });
        </script>
        <!-- /.site-footer -->
        <footer class="site-footer">
            <div class="mr-auto">
              Prueba PHP - Juan Moreno
            </div>
        </footer>
@endsection
