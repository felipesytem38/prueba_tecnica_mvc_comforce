@extends('layouts.app')
@section('content')
<div class="container" style="text-align: center;">
  @if(session('msj'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
              {{session('msj')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
      </div>
  @endif
  @if(session('msjerror'))
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
              {{session('msjerror')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
      </div>
  @endif
</div>
<div class="container">
  <h3 style="text-align: center; margin-top: 8px; margin-bottom: 1%;">Actualizar Contraseña de Usuario</h3>
    <div class="row" style="width: 100%;">
        <div class="col-md-8 col-md-offset-2" style="margin-left: 34%;">
            <div class="panel panel-default">

                <div class="panel-body">
                    <form method="POST" action="{{ route('saveNewPassword') }}" style="font-size: 16px; color: black;">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('contraseña-actual') ? ' has-error' : '' }}">
                            <label style="margin-left: 13%;" for="new-password" class="col-md-4 control-label">Contraseña Actual</label>

                            <div class="col-md-6">
                                <input id="contraseña-actual" type="password" style="border-color: black;" class="form-control" name="contraseña-actual" required>

                                @if ($errors->has('contraseña-actual'))
                                    <span class="help-block" style="color: red;">
                                        <strong>{{ $errors->first('contraseña-actual') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('nueva-contraseña') ? ' has-error' : '' }}">
                            <label style="margin-left: 13%;" for="nueva-contraseña" class="col-md-4 control-label">Nueva Contraseña</label>

                            <div class="col-md-6">
                                <input id="nueva-contraseña" type="password" style="border-color: black;" class="form-control" name="nueva-contraseña" required>

                                @if ($errors->has('nueva-contraseña'))
                                    <span class="help-block" style="color: red;">
                                        <strong>{{ $errors->first('nueva-contraseña') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label style="margin-left: 9%;" for="nueva-contraseña-confirm">Confirmar Nueva Contraseña</label>

                            <div class="col-md-6">
                                <input id="nueva-contraseña-confirm" type="password" style="border-color: black; margin-bottom: 10%;" class="form-control" name="nueva-contraseña_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" style="margin-left: 18%; padding: 6%; border-radius: 28px;" id="botones" class="btn btn-primary">Actualizar Contraseña
                                  <i style="margin-left: 5px; vertical-align: middle;" class="material-icons">update</i>
                                </button>
                                <a class="btn btn-outline-danger py-2 mt-2" id="boton_menu" href="{{ route('home') }}">
                                    Volver al Menú Principal<i style="margin-left: 5px; vertical-align: middle;" class="material-icons">home</i>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
