<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las contraseñas deben tener al menos seis caracteres y coincidir en la confirmación.',
    'reset' => 'Su contraseña se ha reestablecido correctamente',
    'sent' => 'Se ha enviado un enlace de recuperación de contraseña al correo electronico ingresado!',
    'token' => 'EL token de reestablecimiento de contraseña es invalido.',
    'user' => "El correo electrónico ingresado no esta registrado en el sistema.",

];
